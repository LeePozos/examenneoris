package com.example.examenNeoris.Api;
import com.example.examenNeoris.Empleado.*;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="cliente")
public class ClienteController {

	@RequestMapping(value="/inicio", method=RequestMethod.GET)
	public ModelAndView inicio() {
		
		ModelAndView view = new ModelAndView("cliente");
		
		ClienteDao dao = new ClienteDao();
		List<Cliente> listCliente = new ArrayList<Cliente>();
		listCliente=dao.listarCliente();
		return new ModelAndView ("cliente", "list", listCliente);
		
	}
	
	@RequestMapping(value = "/metodos", method = RequestMethod.POST)
  public String authenticateUser(@RequestParam("id") String id,@RequestParam("nombre") String nombre,
  		@RequestParam("edad") String edad, @RequestParam("direccion") String direccion) {       
		
		
		ClienteDao dao= new ClienteDao();
		Cliente cliente= new Cliente();
		
		cliente.setId(Integer.parseInt(id));
		cliente.setNombre(nombre);
		cliente.setEdad(Integer.parseInt(edad));
		cliente.setDireccion(direccion);
		cliente.setGenero(direccion);
	
		
		
		dao.guardar(cliente);
		return "cliente";

}
	
	@RequestMapping(value = "/metodos2", method = RequestMethod.POST)
  public String authenticateUser(@RequestParam("id") String id) {       
		
		
		ClienteDao dao= new ClienteDao();
		Cliente cliente= new Cliente();
		
		cliente.setId(Integer.parseInt(id));
		
		dao.buscar(cliente);
		
		return "cliente";

}
   @RequestMapping(value = "/metodos3", method = RequestMethod.POST)
  public String authenticateUser2(@RequestParam("id") String id,@RequestParam("nombre") String nombre,
  		@RequestParam("edad") String edad, @RequestParam("direccion") String direccion) {       
		
		
		ClienteDao dao= new ClienteDao();
		Cliente cliente= new Cliente();
		
		cliente.setId(Integer.parseInt(id));
		cliente.setNombre(nombre);
		cliente.setEdad(Integer.parseInt(edad));
		cliente.setDireccion(direccion);
	
	
		
		dao.editar(cliente);
		return "cliente";

}
  
}