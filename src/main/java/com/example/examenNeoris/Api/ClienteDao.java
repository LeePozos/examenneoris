package com.example.examenNeoris.Api;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.example.examenNeoris.Empleado.Cliente;

public class ClienteDao implements ClienteOp{

	@Override
	public void guardar(Object object) {
		Cliente cliente = (Cliente) object;
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("leepozos");
		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();

		try {

			cliente.setNombre(cliente.getNombre());
			cliente.setEdad(cliente.getEdad());
			cliente.setGenero(cliente.getGenero());
			cliente.setDireccion(cliente.getDireccion());

			em.persist(cliente);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
		}
		
	}

	@Override
	public void buscar(Object object) {
		
		Cliente cliente = (Cliente) object;
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("leepozos");
		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();
		

		try {
			Cliente cliB = em.find(Cliente.class, cliente.getId());
			cliB.getId();
			cliB.getNombre();
			
		
			em.persist(cliB);
			em.getTransaction().commit();
			System.out.println("cliente : "+cliB);
		} catch (Exception e) {
			em.getTransaction().rollback();
			System.out.println("error en el buscar -> " + e.getMessage());
		}
	}

	@Override
	public void editar(Object object) {
		Cliente cliente = (Cliente) object;
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("leepozos");
		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();

		try {

			Cliente cliE = em.find(Cliente.class, cliente.getId());
			
			cliE.setNombre(cliente.getNombre());
			cliE.setEdad(cliente.getEdad());
			cliE.setGenero(cliente.getGenero());
			cliE.setDireccion(cliente.getDireccion());
			
			em.persist(cliE);
			em.getTransaction().commit();
		}catch (Exception e) {
			
		}
		
	}

	@Override
	public void eliminar(Object object) {
		Cliente cliente = (Cliente) object;
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("leepozos");
		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();

		Cliente cliD = em.find(Cliente.class, cliente.getId());

		try {
			em.remove(cliD);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			System.out.println("error en el delete -> " + e.getMessage());
		}

		
	}

	@Override
	public List listarCliente() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("leepozos");
		EntityManager em = emf.createEntityManager();

		List<Cliente> listCliente = (List<Cliente>) em.createQuery("FROM Cliente").getResultList();
		System.out.println("tus Caricaturas en la base de datos son->" + listCliente);
		return listCliente;
	}

}
