package com.example.examenNeoris.Api;

import java.util.List;




public interface ClienteOp  {
  
	public void guardar(Object object);
	public void buscar(Object object);
	 public void editar(Object object);
	 public void eliminar(Object object);	 
	 public List listarCliente();

}