package com.example.examenNeoris.Empleado;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;




@Entity
public class Cuenta {
    
    @Id
    @EmbeddedId
    private String numeroCuenta;
    
    @Column
    private String tipoCuenta;
    
    @Column
    private Double saldoInicial;
    
    @Column
    private boolean estado;
    
    public Cuenta() {
        
    }
    
    public Cuenta(String numeroCuenta, String tipoCuenta, Double saldoInicial, boolean estado) {
        this.numeroCuenta = numeroCuenta;
        this.tipoCuenta = tipoCuenta;
        this.saldoInicial = saldoInicial;
        this.estado = estado;
    }
    
    public String getNumeroCuenta() {
        return numeroCuenta;
    }
    
    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }
    
    public String getTipoCuenta() {
        return tipoCuenta;
    }
    
    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }
    
    public Double getSaldoInicial() {
        return saldoInicial;
    }
    
    public void setSaldoInicial(Double saldoInicial) {
        this.saldoInicial = saldoInicial;
    }
    
    public boolean isEstado() {
        return estado;
    }
    
    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cuenta)) return false;
        Cuenta cuenta = (Cuenta) o;
        return Objects.equals(getNumeroCuenta(), cuenta.getNumeroCuenta());
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(getNumeroCuenta());
    }
}
