package com.example.examenNeoris;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExamenNeorisApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamenNeorisApplication.class, args);
	}

}
